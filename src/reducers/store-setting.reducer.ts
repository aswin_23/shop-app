const Initial_Setting = {
  cities: {},
  flashsales_banner: [],
};

export default (state = Initial_Setting, action: any) => {
  switch (action.type) {
    case "store-setting":
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
