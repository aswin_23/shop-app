const Initial_User = {
  country_code: null,
  dob: null,
  email: "",
  email_editable: false,
  firstname: "",
  gender: "",
  lastname: "",
  mobile: "",
};

export default (state = Initial_User, action: any) => {
  switch (action.type) {
    case "Set_User_Data":
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
