import { combineReducers } from "redux";
import configReducer from "./config.reducer";
import settingsReducer from "./store-setting.reducer";
import userReducer from "./user.reducer";
import widgetReducer from "./widget.reducer";
import productReducer from "./product.reducer";

export default combineReducers({
  configData: configReducer,
  storeSetting: settingsReducer,
  userData: userReducer,
  widgetData: widgetReducer,
  selectedProduct: productReducer,
});
