const Init_Product = {
  name: "",
  product_sku: "",
  manufacturer: "",
  slug: "",
  price: "",
  final_price: "",
  currency_code: "",
  media_gallery: [
    {
      thumb: "",
      large: "",
    },
  ],
  image: "",
  warranty_badge_icon: "",
  current_city: "",
  related_product: [{}],
  bundle_suggestion: [{}],
};

export default (state = Init_Product, action: any) => {
  switch (action.type) {
    case "product_detail":
      return { ...state, ...action.payload };
    case "related_products":
      state.related_product = action.payload;
      return { ...state };
    case "bundle_suggestion":
      state.bundle_suggestion = action.payload;
      return { ...state };
    default:
      return state;
  }
};
