const Initial_Config = {
  banners: {},
  config: {},
  footer: {},
  menu: [],
};

export default (state = Initial_Config, action: any) => {
  switch (action.type) {
    case "Config":
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
