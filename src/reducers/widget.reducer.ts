const Initial_Widget = {
  new_arrivals: {},
  top_selling: {},
  recommended: [],
};

export default (state = Initial_Widget, action: any) => {
  switch (action.type) {
    case "set-widget":
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
