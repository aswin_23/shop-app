import { Dispatch } from "redux";
import api from "../services/interseptor";

const storeSetting = () => (dispatch: Dispatch) => {
  api.get("/rest/default/V1/site/store-settings").then((data: any) => {
    const settingData = data?.pop();
    dispatch({
      type: "store-setting",
      payload: settingData,
    });
  });
};

export default storeSetting;
