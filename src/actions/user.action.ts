import { Dispatch } from "redux";

import api from "../services/interseptor";

const getUser = () => (dispatch: Dispatch) => {
  api.get("/rest/default/V1/customer/profile").then((data: any) => {
    const customeData = data.pop();
    dispatch({
      type: "Set_User_Data",
      payload: customeData,
    });
  });
};

export default getUser;
