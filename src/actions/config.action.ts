import { Dispatch } from "redux";
import api from "../services/interseptor";

const config = () => (dispatch: Dispatch) => {
  api.get("/rest/default/V1/site/settings").then((data: any) => {
    const configData = data?.pop();
    dispatch({
      type: "Config",
      payload: configData,
    });
    api.get("/rest/default/V2/product/widgets").then((data: any) => {
      const widget = data?.pop();
      dispatch({
        type: "set-widget",
        payload: widget,
      });
    });
  });
};

export default config;
