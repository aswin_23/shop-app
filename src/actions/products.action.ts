import { Dispatch } from "redux";

import api from "../services/interseptor";
import { Support } from "../services/support.service";

const getProductDetails = (sku: any) => (dispatch: Dispatch) => {
  const detailUrl = Support.getCookie("user_token")
    ? `/rest/default/V2/customer/product/detail/`
    : `/rest/default/V2/product/detail/`;
  const bundleUrl = Support.getCookie("user_token")
    ? `/rest/default/V1/customer/bundle/suggestion/`
    : `/rest/default/V1/bundle/suggestion/`;
  api.get(`${detailUrl + sku}?city=riyadh`).then((data: any) => {
    const productDetails = data?.pop();
    dispatch({
      type: "product_detail",
      payload: productDetails,
    });
    api
      .get(`/rest/default/V2/product/related/${sku}`)
      .then((relatedProducts: any) => {
        dispatch({
          type: "related_products",
          payload: relatedProducts,
        });
        api.get(`${bundleUrl + sku}`).then((relatedProducts: any) => {
          dispatch({
            type: "bundle_suggestion",
            payload: relatedProducts,
          });
        });
      });
  });
};

export default getProductDetails;
