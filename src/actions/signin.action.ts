import { Dispatch } from "redux";

import api from "../services/interseptor";
import { Support } from "../services/support.service";
import getUser from "./user.action";

const signIn = (userData: any) => (dispatch: Dispatch | any) => {
  api
    .post("/rest/default/V1/customer/login", userData)
    .then((authToken: any) => {
      Support.setCookie("user_token", authToken);
      dispatch(getUser());
    });
};

export default signIn;
