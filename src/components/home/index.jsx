import React from "react";

import { Banner } from "../banner";
import { CategoryMenu } from "../category-menu";
import { ProductSlider } from "../products/product-slider";

export const Home = (props) => {
  return (
    <>
      <Banner bannerData={props.bannerData} />
      <div className="container">
        <CategoryMenu catMenu={props.catMenu} />
        <ProductSlider
          products={props.widgetData.new_arrivals.products}
          slug={
            props.widgetData.new_arrivals.category_info &&
            props.widgetData.new_arrivals.category_info.slug
          }
          title="landing_top_seller"
        />
      </div>
    </>
  );
};
