import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withTranslation } from "react-i18next";
import { Navbar, Nav, Dropdown } from "react-bootstrap";

import storeSetting from "../../actions/store-setting.action";
import Login from "../../components/user/login";
import translate from "../../services/translate.service";

class Header extends Component {
  render() {
    return (
      <div id="fixed-header">
        <Navbar
          expand="lg"
          variant="dark"
          className="header-nav p-sm-0 bg-header"
        >
          <div className="container">
            <Link to="/" className="navbar-brand">
              <img src={this.props.logo} alt="logo_alt" />
            </Link>
            <Nav>
              <Nav.Link>
                <button
                  className="btn btn-link"
                  onClick={() => {
                    translate.changeLanguage(
                      translate.language === "en" ? "ar" : "en"
                    );
                  }}
                >
                  {translate.t("language")}
                </button>
              </Nav.Link>
              <Nav.Link>
                <Login />
              </Nav.Link>
            </Nav>
          </div>
        </Navbar>
        <div className="topmenu">
          <div className="container">
            <Nav as="ul">
              {this.props.menu.map((item) =>
                item.children && item.children.length ? (
                  <Nav.Item as="li" key={item.slug}>
                    <Dropdown>
                      <Dropdown.Toggle as="a" className="nav-link">
                        {item.name}
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {item.children.map((submenu) => (
                          <React.Fragment key={submenu.slug}>
                            <Dropdown.Item
                              href={`/product/catalog/${submenu.slug}`}
                            >
                              <b>{submenu.name}</b>
                            </Dropdown.Item>
                            {submenu.children &&
                              submenu.children.map((item) => (
                                <Dropdown.Item
                                  href={`/product/catalog/${item.slug}`}
                                  key={item.slug}
                                >
                                  {item.name}
                                </Dropdown.Item>
                              ))}
                          </React.Fragment>
                        ))}
                      </Dropdown.Menu>
                    </Dropdown>
                  </Nav.Item>
                ) : (
                  <Nav.Item as="li" key={item.slug}>
                    <Nav.Link href={`/product/catalog/${item.slug}`}>
                      {item.name}
                    </Nav.Link>
                  </Nav.Item>
                )
              )}
            </Nav>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default withTranslation()(
  connect(mapStateToProps, storeSetting)(Header)
);
