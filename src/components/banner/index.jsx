import React from "react";
import Slider from "react-slick";

import "./banner.scss";

export const Banner = ({ bannerData }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    pauseOnHover: true,
  };
  return (
    <Slider {...settings}>
      {bannerData?.data.map((item) => (
        <img src={item.largeimage} alt="banner item" key={item.sortorder} />
      ))}
    </Slider>
  );
};
