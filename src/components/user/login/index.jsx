import React, { Component } from "react";
import { Form, Field } from "react-final-form";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import { Support } from "../../../services/support.service";
import translate from "../../../services/translate.service";
import signIn from "../../../actions/signin.action";
import getUser from "../../../actions/user.action";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
    };
  }

  componentDidMount() {
    if (Support.getCookie("user_token")) {
      this.props.userProfile();
    }
  }

  render() {
    return Support.getCookie("user_token") ? (
      <div>Hi, {this.props.userData.firstname}</div>
    ) : (
      <>
        <button
          className="btn btn-link"
          onClick={() => this.setState({ isModalOpen: true })}
        >
          {translate.t("login")}
        </button>

        <Modal
          show={this.state.isModalOpen}
          onHide={() => this.setState({ isModalOpen: false })}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>{translate.t("login")}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form
              onSubmit={this.props.onSignIn}
              render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                  <Field name="username">
                    {({ input, meta }) => (
                      <div className="form-group">
                        <label>Username</label>
                        <input
                          {...input}
                          type="text"
                          className="form-control"
                          placeholder="Username"
                        />
                        {(meta.error || meta.submitError) && meta.touched && (
                          <span>{meta.error || meta.submitError}</span>
                        )}
                      </div>
                    )}
                  </Field>
                  <Field name="password">
                    {({ input, meta }) => (
                      <div className="form-group">
                        <label>Password</label>
                        <input
                          {...input}
                          type="password"
                          placeholder="Password"
                          className="form-control"
                        />
                        {meta.error && meta.touched && (
                          <span>{meta.error}</span>
                        )}
                      </div>
                    )}
                  </Field>
                  <div className="buttons">
                    <button className="btn btn-primary" type="submit">
                      Log In
                    </button>
                  </div>
                </form>
              )}
            />
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = {
  onSignIn: signIn,
  userProfile: getUser,
};

export default withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(Login)
);
