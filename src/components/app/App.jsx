import React, { Component } from "react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import config from "../../actions/config.action";
import "./App.scss";
import Header from "../header";
import Approutes from "../../services/route.service";
import translate from "../../services/translate.service";

class App extends Component {
  render() {
    return (
      <div className={translate.language === "ar" ? "rtl" : null}>
        <Header
          logo={this.props.configData.config.logo_url}
          menu={this.props.configData.menu.filter(
            (item) => item.visibility && item.children
          )}
        />
        <Approutes
          catMenu={this.props.configData.menu.filter(
            (item) => item.visibility && item.children
          )}
          bannerData={this.props.configData.banners.banner}
          widgetData={this.props.widgetData}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default withTranslation()(connect(mapStateToProps, config)(App));
