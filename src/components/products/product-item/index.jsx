import React from "react";
import { Link } from "react-router-dom";

import "./product-item.scss";

export const ProductItem = (props) => (
  <div className="productwrap">
    <Link to={`/product/detail/${props.product.slug}`}>
      <div className="row product-head">
        <div className="col-6 warrent-wrap">
          {props.product?.warranty_badge_icon && (
            <img
              src={props.product?.warranty_badge_icon}
              className="warranty-badge"
              alt=""
            />
          )}
        </div>
      </div>
      <div className="product-image">
        <img src={props.product.thumb_image} className="js-img-lazy" alt="" />
      </div>
      <div className="product-detail">
        <h2 className="product-name">{props.product?.name}</h2>
        <p className="product-manufacturer">
          {props.product?.custom_attributes[0]?.manufacturer}
        </p>
        <div className="product-price-wrap" dir="rtl">
          {/* <div *ngIf="getDiscountPercentage()" className="product-price-strick">
          {{props.product.price}} <span>{{props.product.currency_code}}</span>
        </div> */}
          <div className="product-price">
            {props.product.final_price}{" "}
            <span>{props.product.currency_code}</span>
          </div>
        </div>
      </div>
    </Link>
  </div>
);
