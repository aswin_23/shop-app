import React from "react";
import Slider from "react-slick";
import { useTranslation } from "react-i18next";

import { ProductItem } from "../product-item";
import { Link } from "react-router-dom";

import "./product-slider.scss";

export const ProductSlider = (props) => {
  const { t } = useTranslation();
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrow: true,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
  };
  return (
    <div className="product-list-wrap my-3">
      <div className="product-title">
        <h2>{t(props.title)}</h2>
        <Link to={`/product/catalog/${props.slug}`}>
          {t("homepage_view_all")}
        </Link>
      </div>
      <Slider {...settings}>
        {props.products &&
          props.products.map((product) => (
            <ProductItem product={product} key={product.id} />
          ))}
      </Slider>
    </div>
  );
};
