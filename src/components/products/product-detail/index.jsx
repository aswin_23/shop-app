import React, { Component } from "react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import translate from "../../../services/translate.service";
import getProductDetails from "../../../actions/products.action";
import "./product-detail.scss";

class ProductDetail extends Component {
  componentDidMount() {
    const sku = this.props.location.pathname.substring(
      this.props.location.pathname.lastIndexOf("-") + 1,
      this.props.location.pathname.indexOf(".html")
    );
    this.props.getDetails(sku);
  }

  render() {
    return (
      <div className="white-panel detail-panel card-panel">
        <div className="row">
          <div className="col-12 col-lg-4 mb-3 mb-lg-0">
            <div className="left-detail-panel">
              <div className="product-big-image">
                <img
                  className="loading"
                  src={this.props.selectedProduct.image}
                  alt=""
                />
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-8">
            <div className="right-detail-panel product-info-detail-section">
              <div className="row">
                <div className="col-12 col-sm-10 col-md-9 col-lg-10">
                  <h1>{this.props.selectedProduct.name}</h1>
                  <div className="manufacturing-info mt-3">
                    <span className="manufacturer">
                      {this.props.selectedProduct?.manufacturer}
                    </span>
                    <p className="d-block">
                      <span className="manufacturing-number">
                        {translate.t("manufacturer_no")}:&nbsp;
                      </span>
                      <span className="manufacture-no">
                        {this.props.selectedProduct?.item_inventory_code}
                      </span>
                    </p>
                    <p className="d-block">
                      <span className="stock-unit-name">
                        {translate.t("sku")}:&nbsp;
                      </span>
                      <span className="stock-unit-no">
                        {this.props.selectedProduct?.product_sku}
                      </span>
                    </p>
                  </div>
                  <div className="product-cost">
                    <div className="pdt-detail-cost">
                      <span className="currency">
                        {this.props.selectedProduct.currency_code}
                      </span>
                      <span className="price">
                        {this.props.selectedProduct.final_price
                          ? this.props.selectedProduct.final_price
                          : this.props.selectedProduct.price}
                      </span>
                    </div>
                    {this.props.selectedProduct.final_price && (
                      <div className="product-cost-strick mx-2">
                        <span className="currency">
                          {this.props.selectedProduct.currency_code}
                        </span>
                        <span className="price">
                          {this.props.selectedProduct.price}
                        </span>
                      </div>
                    )}
                    <p className="priceIncludeVat">
                      {translate.t("price_vat_label")}
                    </p>
                  </div>
                </div>
                <div className="col-4 col-sm-2 col-md-3 col-lg-2 warranty-wrap">
                  <img
                    src={this.props.selectedProduct.warranty_badge_icon}
                    className="warranty-badge"
                    alt=""
                  />
                </div>
              </div>
              {this.props.selectedProduct.is_out_of_stock && (
                <div className="no-stock-notify mt-1">
                  <h3>
                    <i className="mystore-soldout"></i>&nbsp;
                    {translate.t("out_of_stock")}
                  </h3>
                </div>
              )}
              <div className="pdt-specifications">
                <div className="qty-wrap">
                  <label className="prod-quantity-label">
                    {translate.t("cart_quantity_label")}
                  </label>

                  <div className="qty-buttons">
                    <button className="btn decQty">
                      <span className="icon-minus"></span>
                    </button>
                    <label className="qtyLabel">4</label>
                    <button className="btn incQty">
                      <span className="icon-plus"></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = {
  getDetails: getProductDetails,
};

export default withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(ProductDetail)
);
