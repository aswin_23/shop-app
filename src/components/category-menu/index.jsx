import React from "react";

import "./category-menu.scss";

export const CategoryMenu = ({ catMenu }) => (
  <div className="category-list d-flex mt-2">
    {catMenu.map((category) => (
      <a
        href={`/product/catalog/${category.slug}`}
        className="category-list-item"
        key={category.slug}
      >
        <div className="category-img">
          <img src={category.web_category_image} alt="category icon" />
        </div>
        <h4 className="category-name">{category.name}</h4>
      </a>
    ))}
  </div>
);
