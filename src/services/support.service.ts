import { Cookies } from "react-cookie";

class Supporters {
  private history: any;
  private cookie: any;

  constructor() {
    this.initCookie();
  }

  initCookie() {
    if (!this.cookie) {
      this.cookie = new Cookies();
    }
  }

  setCookie(key: string, value: string) {
    this.cookie.set(key, value, "/");
  }

  getCookie(key: string) {
    return this.cookie.get(key) || null;
  }

  removeCookie(key: string) {
    this.cookie.remove(key);
  }
}

export const Support = new Supporters();
