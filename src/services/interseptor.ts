import axios from "axios";
import { Support } from "../services/support.service";

const api = axios.create({
  baseURL: "https://qa.mystore.com.sa/",
  headers: {
    "api-key": "9769482e1fc87001f2a12f4e722a66e5",
    "api-version": "1.0",
    "content-type": "application/json",
    authorization: "Bearer 3f4a3x10im2cpjkq95kakg6kop1w0w7b",
  },
});

api.interceptors.request.use(
  (axiosConfig) => {
    let useHeaders = {
      ...axiosConfig.headers,
    };
    if (Support.getCookie("user_token")) {
      useHeaders["authorization"] = `Bearer ${Support.getCookie("user_token")}`;
    }
    return {
      ...axiosConfig,
      headers: useHeaders,
    };
  },
  (error) => Promise.reject(error)
);

api.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    console.log(error);
    return error;
  }
);

export default api;
