import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { Home } from "../components/home";
import ProductDetail from "../components/products/product-detail";
import { NotFound } from "../components/not-found";

const Approutes = (props: any) => (
  <Switch>
    <Route
      path="/"
      exact
      render={() =>
        <Home {...props} />
      }
    />
    <Route
      path="/product/detail/:slug"
      render={(routeProps) => <ProductDetail {...routeProps} />}
    />
    <Route path="*" component={NotFound} />
  </Switch>
);

export default withRouter(connect(null)(Approutes));
